# Next.js Crash Course Project

Project from my Next.js crash course  to undestand the new next.js features

### Run the development server:

```bash
npm run dev
```

### Build for production AND generate static website into "out" folder:

```bash
npm run build
```

### Run in production:

```bash
npm start
```

### API Routes:

GET /api/articles
GET /api/articles/:id

## Video Tutorial ###
https://www.youtube.com/watch?v=mTz0GXj8NN0


### important --> api/articles/[id].js ##
if you want to create a path without using parameters from the parent component:

For Example:

let's suppose that we want to allow the user that he will be capable of set any details list just typing the URL path in the browser (not clicking a list), well this is the way (below listed):

* getStaticProps: fetching data on building time (you need to use getStaticPaths mandatoru¡y to use it )
* getStaticPaths : generating dymamic path WITHOUT the data
* const router = useRouter() and const { id } = router.query: Creating path through parameters from the parent component

### important --> How to deploy static page/project ##
* on package.json you need to set "next export":   
  - "build": "next build && next export",

* It will generate a folder "build" which is the project that your able to pull down to production

* tutorial: https://youtu.be/mTz0GXj8NN0?t=2988